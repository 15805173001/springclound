package com.luo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <Description>
 *
 * @author luohongbing
 * @date 2019/07/28 21:22
 */
@Slf4j
@RestController
@RequestMapping("/member")
public class MemberController {

//    @Autowired
//    public MemberClient memberClient;

    @GetMapping("/getMemberName")
    public String getMemberName(){
        try{
            int i = 1/0;
            return "张三";
        }catch (Exception e){
            log.error("异常 name:{}","罗红兵",e);
        }
        return null;

    }

    /*@GetMapping("/test")
    public String test(){
        return memberClient.getMemberName();
    }*/


}
