package com.luo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * eureka 注册中心
 *
 * @author: 罗红兵
 * date:    2019/7/27 17:22
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaApplication1 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication1.class, args);
    }

}
