package com.luo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/8/7 18:03
 */
@Slf4j
public class Job {

    @Scheduled(cron = "0/5 * * * * * *")
    public void test(){
        log.info("测试定时任务");
    }
}
